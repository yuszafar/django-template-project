import time
from django.utils.deprecation import MiddlewareMixin



class RequestMiddleware(MiddlewareMixin):



    def process_request(self, request):
        request.start_time = time.time()
        

    def process_response(self, request, response):
        return response